# Online Restaurant WebApp: AACI

The goal of this project is to build an web application using Django for an online restaurant. Users will be able to browse the restaurantís menu, add items to their cart, and submit their orders. Meanwhile, the restaurant's chefs will be able to add and update menu items. Surfers will be able to view the menu but they can't order items. Surfers can become registered users if they deposit $100

On this web app, users can :
* Login, browse the menu and items to their cart.
* Once an order is complete, user can decide to place an order.
* Deposit money (Finance -> Deposit)

On this web app, users can :
*Surfers can view menu and can become registered users if they deposit $100

# How to run this application

Steps:

1.Activate the virtual environment:
	source oo_env/bin/activate

2.pip install  django-crispy-forms

3.pip install Pillow

4.pip install django-money

5.python manage.py makemigrations

6.python manage.py migrate

7.python manage.py runserver or  you can use python3 instead of python

8.Copy paste the URL link to your web browser

9.Starting development server at http://127.0.0.1:8000/


# How to run the admin interface

Super user can be created with the following
 
python manage.py createsuperuser
 
While the application is running on your local computer, go tho this url: http://127.0.0.1:8000/admin/



These are potential users and their account information


vip customer
username :   user0
password :   user0user0
regular customer   username : user1
pasword : user1user1
admin username : AACIadmin password : AACIpassword

BACKUP REPOSITORY : https://Gr8_gOOse@bitbucket.org/Gr8_gOOse/csc-322-project-complete.git