{% extends "layout.html" %}
{% load crispy_forms_tags %}
{% load static %}


{% block content %}
<link rel="stylesheet" type="text/css" href="{% static 'main.css' %}">
<main role="main" class="container">
    <div class="row">
        <div class="col-md-8">
            {% for post in posts %}
                <article class="media content-section">
                  <img class="rounded-circle article-img" src="{{ post.author.profile.image.url }}">
                  <div class="media-body">
                    <div class="article-metadata">
                      <a class="mr-2" href="#">{{ post.author }}</a>
                      <small class="text-muted">{{ post.date_posted|date:"F d, Y" }}</small>
                    </div>
                    <h2><a class="article-title" href="{% url 'post-detail' post.id %}">{{ post.title }}</a></h2>
                    <p class="article-content">{{ post.content }}</p>
                  </div>
                </article>
            {% endfor %}

        </div>
        <div class="col-md-4">
            <div class="content-section">
                <h3>Discussion Sidebar</h3>
                <p class='text-muted'>Start a Discussion
                    <a href="{% url "post-create" %}">
                        <button class="btn btn-outline-primary mx-1 my-xl-0 my-2">New Discussions</button>
                    </a>
                  <ul class="list-group">
                    <li class="list-group-item list-group-item-light">Latest Posts</li>
                    <li class="list-group-item list-group-item-light">Announcements</li>
                    <li class="list-group-item list-group-item-light">Calendars</li>
                    <li class="list-group-item list-group-item-light">etc</li>
                  </ul>
                </p>
            </div>
        </div>
    </div>
</main>
{% endblock %}

















from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Post
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)

def home(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'discussion/discussion.html', context)



class PostListView(ListView):
    model = Post
    template_name = 'discussion/discussion.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']




class PostDetailView(DetailView):
    model = Post




class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)




class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False



class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/discussion'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

















from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})







