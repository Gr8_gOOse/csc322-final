from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import *
from orders.models import*
from heapq import nlargest

# Create your views here.

def register_view(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = RegistrationForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password1"]
                user = authenticate(username=username, password=password)
                messages.success(request, f'Account created for {username}!')
                login(request, user)
                return redirect("index")
        else:
            form = RegistrationForm()
        context = {"form": form}
        return render(request, 'users/register.html', context)
    else:
        return redirect("index")



@login_required
def profile(request):

    #create a default profile if user doesn't have a profile
    if request.method == 'GET':
        if not hasattr(request.user,'profile'):
            newProfile = Profile(user=request.user)
            newProfile.save()
            moneyspent = 0
            totalorders = 0
        else:
            newProfile = Profile.objects.get(user=request.user)
            moneyspent = newProfile.moneyspent
            totalorders= newProfile.totalorders

    #update existing user profile
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)


        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')


    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
        #print("Total orders",request.user.profile.totalorders)
        if 'money' in request.session:
            money = request.session['money']
            moneyspent = money
            orders = request.session['orders']
            totalorders = orders
        else:
            money = newProfile.moneyspent
            totalorders = newProfile.totalorders

        if float(str(moneyspent).replace('$','')) >= 500.00 or totalorders > 49:
            newProfile.vipstate = True
            newProfile.save()
            vipstatus = "You are a VIP :)"
            request.session['VIP'] = newProfile.vipstate

        else:
            newProfile.vipstate = False
            newProfile.save()
            request.session['VIP'] = newProfile.vipstate
            vipstatus = "You are not a VIP :("


    #print("Money spent:",moneyspent)
    #print(float(moneyspent.replace('$','')))
    context = {
        'u_form': u_form,
        'p_form': p_form,
        'amountspent':moneyspent,
        'totalorders':totalorders,
        'vipstatus':vipstatus
    }
    if(ProperOrder.objects.filter(order_client=request.user).count()==0):
        messages.add_message(request, messages.INFO, f"You don't have previous orders. Place an order right now!")
    else:

        #pizza
        regular_pizza= Pizza.objects.filter(add_by= request.user).filter(pizzatype=1).count()
        special_pizza= Pizza.objects.filter(add_by= request.user).filter(pizzatype=2).count()

        #sub
        veggie=Sub.objects.filter(add_by= request.user).filter(subtype=1).count()
        premium_beef=Sub.objects.filter(add_by=request.user).filter(subtype=3).count()
        pastrami=Sub.objects.filter(add_by=request.user).filter(subtype= 4).count()
        tuna=Sub.objects.filter(add_by=request.user).filter(subtype= 5).count()
        turkey=Sub.objects.filter(add_by=request.user).filter(subtype= 6).count()
        chicken=Sub.objects.filter(add_by=request.user).filter(subtype= 7).count()
        beef_brisket=Sub.objects.filter(add_by=request.user).filter(subtype= 8).count()

        #platter
        hummus=Platter.objects.filter(add_by=request.user).filter(plattertype= 3).count()
        dessert=Platter.objects.filter(add_by=request.user).filter(plattertype= 4).count()
        cheese=Platter.objects.filter(add_by=request.user).filter(plattertype= 5).count()
        vegetarian=Platter.objects.filter(add_by=request.user).filter(plattertype= 6).count()

        #pasta
        carbonara = Pasta.objects.filter(add_by=request.user).filter(pastatype= 1).count()
        cacio=Pasta.objects.filter(add_by=request.user).filter(pastatype= 2).count()
        tortelli=Pasta.objects.filter(add_by=request.user).filter(pastatype= 3).count()

        #salad
        thai=Salad.objects.filter(add_by=request.user).filter(saladtype= 2).count()
        cobb=Salad.objects.filter(add_by=request.user).filter(saladtype= 3).count()
        caesar=Salad.objects.filter(add_by=request.user).filter(saladtype= 4).count()

        times_ordered = {

            "Regular Pizza" : regular_pizza,
            "Special Pizza": special_pizza,


            "Veggie" : veggie,
            "Premium Roast Beef": premium_beef,
            "Pastrami":pastrami,
            "Tuna Salad":tuna,
            "Smoked Turkey Breast":turkey,
            "Grilled Chicken Breast":chicken,
            "Corned Beef Brisket":beef_brisket,



            "Mediterranean Bruschetta Hummus Platter":hummus,
            "Ultimate Dessert Platter":dessert,
            "Cheese and Meat Board Platter":cheese,
            "Vegetarian Mezze Platter":vegetarian,

            "Pasta Carbonara":carbonara,
            "Cacio e Pepe": cacio,
            "Tortelli": tortelli,


            "Thai Salad" : thai,
            "Cobb Salad": cobb,
            "Caesar": caesar


        }

        threeHighest = nlargest(3, times_ordered, key = times_ordered.get)
        top_three_dish = {}
        for val in threeHighest:
                top_three_dish[val] = times_ordered.get(val)

        context.update({'top_three_dish': top_three_dish})

    return render(request, 'users/profile.html', context)





#new line
