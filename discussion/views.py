from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Post, Comment
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)

def home(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'discussion/discussion.html', context)



class PostListView(ListView):
    model = Post
    template_name = 'discussion/discussion.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']




class PostDetailView(DetailView):
    model = Post




class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)




class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False



class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/discussion'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False



class CommentCreateView(LoginRequiredMixin, CreateView):
    model = Comment
    template_name = 'discussion/comment_form.html'
    fields = ['body']
    success_url = '/discussion/'


    def form_valid(self, form):
        form.instance.user_name = self.request.user
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)
