from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from users import views as user_views
#from discussion import views as *

urlpatterns = [
    path("", include("orders.urls")),
    path("register/", user_views.register_view, name= 'register_view'),
    path("profile/", user_views.profile, name= 'profile'),
    path("admin/", admin.site.urls),
    path('discussion/', include('discussion.urls')),
    path("finance/",include('finance.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
