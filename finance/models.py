from djmoney.models.fields import MoneyField
from django.db import models
from django.contrib.auth.models import User


class BankAccount(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    balance = MoneyField(max_digits=14, decimal_places=2, default_currency='USD')

    def __str__(self):
      return f'{self.user.username} -> Balance {self.balance}'
