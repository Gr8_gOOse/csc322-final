from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import BankAccount
from djmoney.money import Money

@login_required
def home_view(request):
    #Generate a bank account for users that already have an account
    if not hasattr(request.user,'bankaccount'):
        newBankAccount = BankAccount(balance=Money(100, 'USD'),user=request.user)
        newBankAccount.save()
    context = {
        'bankaccount': BankAccount.objects.get(user=request.user)
    }
    return render(request, 'home.html',context)
   
@login_required
def deposit_view(request):
    if request.method == 'POST':
        amount_entered = Money(request.POST.get('amount'), 'USD')
        newBalance = request.user.bankaccount.balance + amount_entered
        userBankAccount = BankAccount.objects.get(user=request.user)
        userBankAccount.balance = newBalance
        userBankAccount.save()
        return redirect("home_view")
    return render(request, 'deposit.html')