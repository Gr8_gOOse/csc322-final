from django.urls import path
from .views import home_view, deposit_view

urlpatterns = [
    path('', home_view,name = 'home_view'),
    path('deposit/', deposit_view,name = 'deposit_view'),
]