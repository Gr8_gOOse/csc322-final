from django.urls import path

from .views import (welcome_page, login_view, logout_view, cart_view,
 menu_view, add_to_cart, make_order, all_orders_view, mark_order_as_done,
  user_orders_view, clear_cart, fill_menu ,ComplaintChefView,
  ComplaintDriverView, ComplimentChefView, ComplimentDriverView, ChefListView,
    DriverListView, ChefCompListView, DriverCompListView
)



urlpatterns = [
    path("", welcome_page , name="index"),
    #path("register/", register_view, name="register_view"),
    path("login/", login_view, name="login_view"),
    path("logout/", logout_view, name="logout_view"),
    path("cart/", cart_view, name="cart_view"),
    path("menu/", menu_view, name="menu_view"),
    path("add/<str:item_type>/<int:item_id>/<str:item_bigger>", add_to_cart, name="add_to_cart"),
    #path("finalize/", make_order, name="make_order" ),
    path("all_orders/", all_orders_view, name="all_orders_view"),
    path("order/<int:order_id>/done", mark_order_as_done, name="mark_order_as_done"),
    path("my_orders/", user_orders_view, name="user_orders_view"),
    path("clear_cart/", clear_cart, name="clear_cart"),
    path("fill_menu/", fill_menu, name="fill_menu"),

    path("finalize/<str:order_type>", make_order, name="make_order"),

    path("complaint_chef/<str:pk>", ComplaintChefView, name="complaint_chef"),
    path("complaint_driver/<str:pk>", ComplaintDriverView, name="complaint_driver"),
    path("compliment_chef/<str:pk>", ComplimentChefView, name="compliment_chef"),
    path("compliment_driver/<str:pk>", ComplimentDriverView, name="compliment_driver"),
    path('chefs/', ChefListView.as_view(), name='chef_list'),
    path('drivers/', DriverListView.as_view(), name='driver_list'),
    path('chefs_complement/', ChefCompListView.as_view(), name='chef_list_comp'),
    path('drivers_complement/', DriverCompListView.as_view(), name='driver_list_comp'),

]


