from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from .models import *
from .forms import *
from django.http import HttpResponseNotFound
from decimal import *
from django.contrib import messages
from finance.models import BankAccount
from djmoney.money import Money
from heapq import nlargest
from random import choice
from django.views.generic import ListView, UpdateView
from users.models import Profile


def delete_all_user_orders(username):
    Sub.objects.filter(add_by=username).filter(already_ordered=False).delete()
    Pasta.objects.filter(add_by=username).filter(already_ordered=False).delete()
    Salad.objects.filter(add_by=username).filter(already_ordered=False).delete()
    Platter.objects.filter(add_by=username).filter(already_ordered=False).delete()
    Pizza.objects.filter(add_by=username).filter(already_ordered=False).delete()
    return True


def calculate_cart_price(username):
    price_all = 0
    for obj in Sub.objects.filter(add_by=username).filter(already_ordered=False):
        price_all += obj.price
    for obj in Pasta.objects.filter(add_by=username).filter(already_ordered=False):
        price_all += obj.price
    for obj in Salad.objects.filter(add_by=username).filter(already_ordered=False):
        price_all += obj.price
    for obj in Platter.objects.filter(add_by=username).filter(already_ordered=False):
        price_all += obj.price
    for obj in Pizza.objects.filter(add_by=username).filter(already_ordered=False):
        price_all += obj.price
    return price_all


def welcome_page(request):

    #pizza
    regular_pizza=Pizza.objects.filter(pizzatype=1).count()
    special_pizza=Pizza.objects.filter(pizzatype=2).count()

    #subs
    veggie=Sub.objects.filter(subtype= 1).count()
    premium_beef=Sub.objects.filter(subtype= 3).count()
    pastrami=Sub.objects.filter(subtype= 4).count()
    tuna=Sub.objects.filter(subtype= 5).count()
    turkey=Sub.objects.filter(subtype= 6).count()
    chicken=Sub.objects.filter(subtype= 7).count()
    beef_brisket=Sub.objects.filter(subtype= 8).count()

    #platter
    hummus=Platter.objects.filter(plattertype= 3).count()
    dessert=Platter.objects.filter(plattertype= 4).count()
    cheese=Platter.objects.filter(plattertype= 5).count()
    vegetarian=Platter.objects.filter(plattertype= 6).count()

    #pasta
    carbonara = Pasta.objects.filter(pastatype= 1).count()
    cacio=Pasta.objects.filter(pastatype= 2).count()
    tortelli=Pasta.objects.filter(pastatype= 3).count()

    #salad
    thai=Salad.objects.filter(saladtype= 2).count()
    cobb=Salad.objects.filter(saladtype= 3).count()
    caesar=Salad.objects.filter(saladtype= 4).count()


    times_ordered = {

        "Regular Pizza" : regular_pizza,
        "Special Pizza": special_pizza,


        "Veggie" : veggie,
        "Premium Roast Beef": premium_beef,
        "Pastrami":pastrami,
        "Tuna Salad":tuna,
        "Smoked Turkey Breast":turkey,
        "Grilled Chicken Breast":chicken,
        "Corned Beef Brisket":beef_brisket,


        "Mediterranean Bruschetta Hummus Platter":hummus,
        "Ultimate Dessert Platter":dessert,
        "Cheese and Meat Board Platter":cheese,
        "Vegetarian Mezze Platter":vegetarian,

        "Pasta Carbonara":carbonara,
        "Cacio e Pepe": cacio,
        "Tortelli": tortelli,



        "Thai Salad" : thai,
        "Cobb Salad": cobb,
        "Caesar": caesar

    }
    threeHighest = nlargest(3, times_ordered, key = times_ordered.get)
    top_three_dish = {}
    for val in threeHighest:
            top_three_dish[val] = times_ordered.get(val)
    return render(request, 'orders/welcome.html',{'top_three_dish': top_three_dish})




def login_view(request):

    if not request.user.is_authenticated:
        if request.method == "POST":
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    #Generate a bank account for users that already have an account
                    if not hasattr(request.user,'bankaccount'):
                        newBankAccount = BankAccount(balance=Money(100, 'USD'),user=request.user)
                        newBankAccount.save()
                    if next is not None:
                        return redirect("index")
                    else:
                        return redirect("index")
        else:
            form = AuthenticationForm()
        context = {"form": form}
        return render(request, 'registration/login.html', context)
    else:
        return redirect("index")


def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("index")


@login_required
def cart_view(request):
    userProfile = Profile.objects.get(user=request.user)
    if userProfile.vipstate:
        cartprice = calculate_cart_price(request.user)
        cartprice = float(cartprice) - 0.10*float(cartprice)
        price_all = round(Decimal(cartprice),2)
    else:
        price_all = Decimal(calculate_cart_price(request.user))
    context = {}
    context.update({"price_all": price_all})
    context.update({"Sub": Sub.objects.filter(add_by=request.user).filter(already_ordered=False)})
    context.update({"Pasta": Pasta.objects.filter(add_by=request.user).filter(already_ordered=False)})
    context.update({"Salad": Salad.objects.filter(add_by=request.user).filter(already_ordered=False)})
    context.update({"Platter": Platter.objects.filter(add_by=request.user).filter(already_ordered=False)})
    context.update({"Pizza": Pizza.objects.filter(add_by=request.user).filter(already_ordered=False)})

    return render(request, 'orders/cart.html', context)


#@login_required()
def menu_view(request):
    form_pizza = PizzaAddForm(request.POST or None)
    if form_pizza.is_valid():
        form_pizza_size = form_pizza.cleaned_data["pizza_size"]
        form_pizza_type = PizzaType.objects.get(name=request.POST["pizzatype"])
        form_pizza_toppings = form_pizza.cleaned_data["toppings"]
        form_pizza_add_by = request.user
        new_pizza = Pizza(add_by=form_pizza_add_by, pizza_size=form_pizza_size, pizzatype=form_pizza_type)
        new_pizza.save()
        new_pizza.toppings.set(form_pizza_toppings)
        new_pizza.calculate_price()
        new_pizza.save()
        messages.add_message(request, messages.INFO, "Pizza added!")
        form_pizza = PizzaAddForm()

    context = {}
    context.update({"Sub": SubType.objects.all()})
    context.update({"Pasta": PastaType.objects.all()})
    context.update({"Salad": SaladType.objects.all()})
    context.update({"Platter": PlatterType.objects.all()})
    context.update({"Pizza": PizzaType.objects.all()})
    context.update({"PizzaToppings": PizzaTopping.objects.all()})
    context.update({"form_pizza": form_pizza})
    return render(request, 'orders/menu.html', context)


@login_required()
def add_to_cart(request, item_type, item_id, item_bigger):
    userProfile = Profile.objects.get(user=request.user)

    if item_type == "sub":
        item_id = SubType.objects.get(id=item_id)
        if (item_id.special and userProfile.vipstate) or (item_id.special != True):
            new_sub = Sub(subtype=item_id, subsize=item_bigger, add_by=request.user)
            new_sub.calculate_price()
            new_sub.save()
            messages.add_message(request, messages.INFO, "Sub added!")
        else:
            messages.add_message(request, messages.INFO, "Must be VIP to add to cart")

    elif item_type == "platter":
        item_id = PlatterType.objects.get(id=item_id)
        if (item_id.special and userProfile.vipstate) or (item_id.special != True):
            new_platter = Platter(plattertype=item_id, plattersize=item_bigger, add_by=request.user)
            new_platter.calculate_price()
            new_platter.save()
            messages.add_message(request, messages.INFO, "Platter added!")
        else:
            messages.add_message(request,messages.INFO, "Must be VIP to add to cart")

    elif item_type == "pasta":
        item_id = PastaType.objects.get(id=item_id)
        if (item_id.special and userProfile.vipstate) or (item_id.special != True):
            new_pasta = Pasta(pastatype=item_id, add_by=request.user)
            new_pasta.calculate_price()
            new_pasta.save()
            messages.add_message(request, messages.INFO, "Pasta added!")
        else:
            messages.add_message(request,messages.INFO, "Musst be VIP to add to cart")

    elif item_type == "salad":
        item_id = SaladType.objects.get(id=item_id)
        if (item_id.special and userProfile.vipstate) or (item_id.special != True):
            new_salad = Salad(saladtype=item_id, add_by=request.user)
            new_salad.calculate_price()
            new_salad.save()
            messages.add_message(request, messages.INFO, "Salad added!")
        else:
            messages.add_message(request,messages.INFO, "Must be VIP to add to cart")

    else:
        return HttpResponseNotFound('<h1>Product not found</h1>')

    return redirect(menu_view)


@login_required()
def make_order(request,order_type):
    #if balance < order price tell user to deposit money
    if Money(calculate_cart_price(request.user),'USD') > request.user.bankaccount.balance:
        messages.add_message(request, messages.INFO, f"Balance = {request.user.bankaccount.balance} < ${calculate_cart_price(request.user)} so please go to finance -> deposit money")
        return redirect(cart_view)
    else:
        userProfile = Profile.objects.get(user=request.user)

        # balance = balance - order_price
        userBankAccount = BankAccount.objects.get(user=request.user)
        #print(Money(calculate_cart_price(request.user),'USD') * 0.10)

        #print("VIP STATE", userProfile.vipstate)
        if userProfile.vipstate:
            cartprice = ((Money(calculate_cart_price(request.user),'USD') - (0.10*Money(calculate_cart_price(request.user),'USD'))))
            userBankAccount.balance = request.user.bankaccount.balance - cartprice
            userBankAccount.save()

        else:
            cartprice = Money(calculate_cart_price(request.user),'USD')
            userBankAccount.balance = request.user.bankaccount.balance - cartprice
            userBankAccount.save()

        userProfile.moneyspent  = request.user.profile.moneyspent + cartprice
        userProfile.totalorders = request.user.profile.totalorders +1
        userProfile.save()
        tempmoney = userProfile.moneyspent
        #print("Total moneyspent = ", userProfile.moneyspent)
        #print("Total orders = ", userProfile.totalorders)

        request.session['money'] = str(tempmoney)
        request.session['orders'] = userProfile.totalorders


        #prepare order
        new_proper_order = ProperOrder()
        new_proper_order.order_client = request.user
        new_proper_order.order_price = calculate_cart_price(request.user)
        new_proper_order.order_type = order_type
        new_proper_order.order_chef = choice(Chef.objects.all())
        if new_proper_order.order_type == 'Delivery' :
            new_proper_order.order_driver = choice(Driver.objects.all())

        new_proper_order.save()

        for item in Sub.objects.filter(add_by=request.user).filter(already_ordered=False):
            item.already_ordered = True
            item.in_order = new_proper_order
            item.save()

        for item in Pasta.objects.filter(add_by=request.user).filter(already_ordered=False):
            item.already_ordered = True
            item.in_order = new_proper_order
            item.save()

        for item in Salad.objects.filter(add_by=request.user).filter(already_ordered=False):
            item.already_ordered = True
            item.in_order = new_proper_order
            item.save()

        for item in Platter.objects.filter(add_by=request.user).filter(already_ordered=False):
            item.already_ordered = True
            item.in_order = new_proper_order
            item.save()

        for item in Pizza.objects.filter(add_by=request.user).filter(already_ordered=False):
            item.already_ordered = True
            item.in_order = new_proper_order
            item.save()

        if order_type == "Delivery":
            messages.add_message(request, messages.INFO, f"Order number {new_proper_order.id} will be out for delivery!")
        elif order_type == "Pick Up":
            messages.add_message(request, messages.INFO, f"Order number {new_proper_order.id} will be ready for pickup!")

        elif order_type == "Dine in":
            messages.add_message(request, messages.INFO, f"Order number {new_proper_order.id} is being prepped to serve")



        #messages.add_message(request, messages.INFO, f"Order number {new_proper_order.id} send!")
        return redirect(user_orders_view)





@staff_member_required
def all_orders_view(request):
    context = {"orders": reversed(ProperOrder.objects.all())}
    return render(request, 'orders/all_orders.html', context)


@login_required
def user_orders_view(request):
    context = {"orders": reversed(ProperOrder.objects.filter(order_client=request.user))}
    return render(request, 'orders/my_orders.html', context)


@staff_member_required
def mark_order_as_done(request, order_id):
    marked = ProperOrder.objects.get(id=order_id)
    marked.order_done = True
    marked.save()
    return redirect(all_orders_view)


@login_required
def clear_cart(request):
    delete_all_user_orders(request.user)
    return redirect("cart_view")


@staff_member_required()
def fill_menu(request):
    add_menu()
    return redirect(welcome_page)





@login_required
def ComplaintChefView(request, pk):
    chef = Chef.objects.get(id=pk)
    form = ChefComplaintForm(instance=chef)
    if request.method == "POST":
        form = ChefComplaintForm(request.POST, instance=chef)
        if form.is_valid():
            form.save()
            return redirect("profile")


    context = {'form':form}
    return render(request, 'orders/complaint_chef.html', context)



@login_required
def ComplaintDriverView(request, pk):
    driver = Driver.objects.get(id=pk)
    form = DriverComplaintForm(instance=driver)
    if request.method == "POST":
        form = DriverComplaintForm(request.POST, instance=driver)
        if form.is_valid():
            form.save()
            return redirect("profile")


    context = {'form':form}
    return render(request, 'orders/complaint_driver.html', context)



@login_required
def ComplimentChefView(request, pk):
    chef = Chef.objects.get(id=pk)
    form = ChefComplimentForm(instance=chef)
    if request.method == "POST":
        form = ChefComplimentForm(request.POST, instance=chef)
        if form.is_valid():
            form.save()
            return redirect("profile")


    context = {'form':form}
    return render(request, 'orders/compliment_chef.html', context)



@login_required
def ComplimentDriverView(request, pk):
    driver = Driver.objects.get(id=pk)
    form = DriverComplimentForm(instance=driver)
    if request.method == "POST":
        form = DriverComplimentForm(request.POST, instance=driver)
        if form.is_valid():
            form.save()
            return redirect("profile")


    context = {'form':form}
    return render(request, 'orders/compliment_driver.html', context)





class ChefListView(ListView):
    model = Chef
    template_name = 'chef_list.html'
    context_object_name = 'chefs'




class DriverListView(ListView):
    model = Driver
    template_name = 'driver_list.html'
    context_object_name = 'drivers'




class ChefCompListView(ListView):
    model = Chef
    template_name = 'chef_complement.html'
    context_object_name = 'chefs'




class DriverCompListView(ListView):
    model = Driver
    template_name = 'driver_complement.html'
    context_object_name = 'drivers'
