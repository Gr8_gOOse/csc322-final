from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Pizza, Sub, Chef, Driver

class PizzaAddForm(forms.ModelForm):

    class Meta:
        model = Pizza
        fields = (
            'pizza_size',
            'toppings',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['toppings'].widget.attrs.update({'size': '7'})



class ChefComplaintForm(forms.ModelForm):

    class Meta:
        model = Chef
        fields = (
            'name',
            'complaints',
            'complement'
        )


class DriverComplaintForm(forms.ModelForm):

    class Meta:
        model = Driver
        fields = (
            'name',
            'complaints',
            'complement',
        )



class ChefComplimentForm(forms.ModelForm):

    class Meta:
        model = Chef
        fields = (
            'name',
            'complement',
        )




class DriverComplimentForm(forms.ModelForm):

    class Meta:
        model = Driver
        fields = (
            'name',
            'complement',
        )









#class RegistrationForm(UserCreationForm):
#    email = forms.EmailField(required=False)

#    class Meta:
#        model = User
#        fields = (
#            'username',
#            'first_name',
#            'last_name',
#            'email',
#            'password1',
#            'password2'
#        )

#   def save(self, commit=True):
#        user = super(RegistrationForm, self).save(commit=False)
#        user.first_name = self.cleaned_data['first_name']
#        user.last_name = self.cleaned_data['last_name']
#        user.email = self.cleaned_data['email']

#        if commit:
#            user.save()

#        return User
